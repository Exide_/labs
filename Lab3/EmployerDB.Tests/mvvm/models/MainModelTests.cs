﻿using NUnit.Framework;
using EmployerDB.mvvm.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using EmployerDB.services;
using Ninject.Modules;
using EmployerDB.ninject;

namespace EmployerDB.mvvm.models.Tests
{
    [TestFixture()]
    public class MainModelTests
    {
        IKernel kernel = null;
        SaleService service = null;

        public MainModelTests()
        {
            // Используем тестовый набор зависимостей
            this.kernel = new StandardKernel(new NinjectTestConfig());
            // Для этого сервиса будет установлено соединение с тестовой базой данных
            this.service = kernel.Get<SaleService>();
        }

        [Test()]
        // Добавим продажу с 2 видами товара, после удалим её
        public void DeleteSaleTest()
        {
            // Запомним число строк в таблицах
            int previousProductInSaleCount = this.service.ProductsInSale.Count();
            int previousSaleCount = this.service.Sales.Count();

            // Подготовим данные для продажи
            int cashierId = 1;
            // Получим список всех продуктов
            Product[] products = this.service.Products.ToArray();

            // И добавим первые 2 в продажу
            Product[] productsInSale = new Product[] { products[0], products[1] };

            // Добавим 3 первых и 5 вторых
            int[] productCountArr = new int[] { 3, 5 };

            // Настроим карту товаров для передачи в функцию
            var productToCountMap = new Dictionary<int, int>();

            for (int i = 0; i < productsInSale.Length; i++)
                productToCountMap.Add(productsInSale[i].Id, productCountArr[i]);


            // Если продажа не добавится, то плюнется исключение. Так и нужно, т.к. тестируем не добавление, а удаление
            this.service.TryAddSale(cashierId, productToCountMap);

            // Продажа добавлена, теперь удаляем её
            Sale saleToDelete = this.service.Sales.Last();
            this.service.DeleteSale(saleToDelete.Id);

            // Получим свежий список продуктов в продажах
            var newProductsInSales = this.service.ProductsInSale;

            int newProductInSaleCount = this.service.ProductsInSale.Count();
            int newSaleCount = this.service.Sales.Count();

            // Убедимся, что число строк стало прежним
            Assert.AreEqual(previousProductInSaleCount, newProductInSaleCount);
            Assert.AreEqual(previousSaleCount, newSaleCount);

            // И убедимся, что ни один id продажи не соответствует id удалённой
            Assert.That(newProductsInSales.All(p => p.SaleId != saleToDelete.Id));

            Assert.Pass();
        }

        [Test()]
        public void TryAddSaleTest()
        {
            // Продажу сделал 1-й кассир
            int cashierId = 1;

            // Получим список всех продуктов
            Product[] products = this.service.Products.ToArray();

            // И добавим первые 2 в продажу
            Product[] productsInSale = new Product[] { products[0], products[1] };

            // Добавим 3 первых и 5 вторых
            int[] productCountArr = new int[] { 3, 5 };


            // Вычислим ожидаемую сумму продажи
            int expectedSum = 0;

            for (int i = 0; i < productsInSale.Length; i++)
                expectedSum += productCountArr[i] * productsInSale[i].Price;

            // Настроим карту товаров для передачи в функцию
            var countMap = new Dictionary<int, int>();

            for (int i = 0; i < productsInSale.Length; i++)
                countMap.Add(productsInSale[i].Id, productCountArr[i]);


            // Вызываем сам метод
            bool success = this.service.TryAddSale(cashierId, countMap);

            Assert.True(success);


            // Получим последнюю продажу
            Sale lastSale = this.service.Sales.Last();

            // Проверим, что её сделал наш кассир
            Assert.That(lastSale.CashierId == cashierId);

            // Получим продукты в продаже
            var storedProductsInSale = this.service.GetProductsInSale(lastSale.Id);

            // Проверим, что записано верное количество видов продуктов
            Assert.AreEqual(storedProductsInSale.Count, productsInSale.Length);

            // Проверим, что количество каждого вида верное
            for (int i = 0; i < productsInSale.Length; i++)
            {
                Product product = productsInSale[i];

                int expectedCount = productCountArr[i];
                int actualCount = storedProductsInSale.First(p => p.Key.Id == product.Id).Value;

                Assert.AreEqual(expectedCount, actualCount);
            }

            // Проверим, что сумма продажи соответствует ожидаемой
            int returnedSaleSum = this.service.GetSaleCost(lastSale.Id);

            Assert.AreEqual(expectedSum, returnedSaleSum);

            // Удалим продажу, потому что почему бы и нет
            this.service.DeleteSale(lastSale.Id);

            Assert.Pass();
        }
    }
}