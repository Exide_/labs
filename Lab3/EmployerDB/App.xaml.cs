﻿using EmployerDB.misc;
using EmployerDB.mvvm.viewmodels;
using EmployerDB.ninject;
using Ninject;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace EmployerDB
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            IKernel kernel = new StandardKernel(new NinjectMainConfig());

            // По канону MVVM контекст должен задаваться именно здесь, а не в XAML
            MainWindow window = new EmployerDB.MainWindow();

            MainViewModel viewmodel = kernel.Get<MainViewModel>();
            window.DataContext = viewmodel;

            window.Show();
        }
    }
}
