﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.mvvm.viewmodels
{
    public class ProductSelectionViewModel: ElementViewModel
    {
        private bool _isSelected = false;
        private int _count = 1;

        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }

        public int Count
        {
            get => _count;
            set => SetProperty(ref _count, value);
        }

        public int Price { get; set; }
    }
}
