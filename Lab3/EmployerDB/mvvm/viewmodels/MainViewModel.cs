﻿using EmployerDB.mvvm.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using static EmployerDB.mvvm.models.MainModel;

namespace EmployerDB.mvvm.viewmodels
{    
    public class MainViewModel: ViewModelBase<MainModel>
    {
        private bool updateFlag = false;
        
        #region Add sale 
        private List<ElementViewModel> _cashiers = null;
        private List<ProductSelectionViewModel> _products = null;
        
        private int _saleCost = 0;
        private int _selectedCashier = 0;

        public List<ElementViewModel> CashierViewModels
        {
            get => _cashiers;
            set => SetProperty(ref _cashiers, value);
        }

        public int SaleCost
        {
            get => _saleCost;
            set => SetProperty(ref _saleCost, value);
        }

        public int SelectedCashierIndex
        {
            get => _selectedCashier;
            set => SetProperty(ref _selectedCashier, value);
        }

        public List<ProductSelectionViewModel> ProductSelectionViewModels
        {
            get => _products;
            set => SetProperty(ref _products, value);
        }
        #endregion

        #region Show products in sale
        private List<SaleViewModel> _sales = null;
        private List<ProductInSaleViewModel> _productsInSale = null;
        private int _selectedSale = -1;

        public int SelectedSaleIndex
        {
            get => _selectedSale;
            set => SetProperty(ref _selectedSale, value);
        }

        public List<SaleViewModel> SaleViewModels
        {
            get => _sales;
            set => SetProperty(ref _sales, value);
        }

        public List<ProductInSaleViewModel> ProductInSaleViewModels
        {
            get => _productsInSale;
            set => SetProperty(ref _productsInSale, value);
        }
        #endregion

        #region Table manager UI
        private DataTable _table = new DataTable();
        private int _selectedTable = -1;
        private int? _idInput = null;
        private DataRow newRow = null;

        public string[] TableNames
        {
            get => this.Model.TableNames;
        }

        public DataTable Table
        {
            get => _table;
            set => SetProperty(ref _table, value);
        }

        public int SelectedTableIndex
        {
            get => _selectedTable;
            set => SetProperty(ref _selectedTable, value);
        }

        public int? IdInput
        {
            get => _idInput;
            set => SetProperty(ref _idInput, value);
        }
        #endregion

        public void Touch()
        {
            this.RaisePropertyChanged(nameof(Table));
        }

        public ICommand ShowIdCommand { get; }
        public ICommand DeleteIdCommand { get; }
        public ICommand CreateSaleCommand { get; }

        public MainViewModel(MainModel model): base(model)
        {
            this.Model.PropertyChanged += (obj, arg) =>
            {
                if (arg.PropertyName == nameof(MainModel.DynamicTable))
                {
                    this.FillTable(this.Model.DynamicTable.Table);
                }
                else if (arg.PropertyName == nameof(MainModel.Cashiers))
                {
                    this.UpdateCashierViewModels();
                }
                else if (arg.PropertyName == nameof(MainModel.Products))
                {
                    this.UpdateProductViewModels();
                }
                else if (arg.PropertyName == nameof(MainModel.Sales))
                {
                    this.UpdateSaleViewModels();
                }
            };

            this.PropertyChanged += (obj, arg) =>
            {
                if (arg.PropertyName == nameof(SelectedTableIndex))
                {
                    this.Model.SelectTable(this.SelectedTableIndex);
                }
                else if (arg.PropertyName == nameof(this.SelectedSaleIndex))
                {
                    this.UpdateProductInSaleViewModels();
                }
            };

            this.ShowIdCommand = new DelegateCommand((obj) =>
            {
                if (this.IdInput != null)
                    this.Model.ShowId((int)this.IdInput);
            });

            this.DeleteIdCommand = new DelegateCommand((obj) =>
            {
                if (this.IdInput != null)
                    this.Model.DeleteId((int)this.IdInput);
            });

            this.CreateSaleCommand = new DelegateCommand((obj) =>
            {
                int cashierPKey = this.CashierViewModels[this.SelectedCashierIndex].PrimaryKey;

                Dictionary<int, int> productMap = new Dictionary<int, int>();

                foreach (ProductSelectionViewModel productVM in this.ProductSelectionViewModels)
                {
                    if (productVM.IsSelected == false) continue;

                    int count = productVM.Count;
                    int productPK = productVM.PrimaryKey;
                    productVM.IsSelected = false;
                    productVM.Count = 1;

                    productMap.Add(productPK, count);
                }

                bool success = this.Model.TryAddSale(cashierPKey, productMap);
                MessageBox.Show(success ? "Успех" : "Ошибка");
            });


            this.Table.RowChanged += (_, arg) =>
            {
                if (this.updateFlag == true) return;

                bool isNew = arg.Row == this.newRow;

                if (isNew == true)
                {
                    List<string> columns = new List<string>();
                    List<object> values = new List<object>();
                    
                    for (int i = 0; i < arg.Row.ItemArray.Length; i++)
                    {
                        string colName = this.Table.Columns[i].ColumnName;
                        // Если для колонки есть значение по умолчанию - пропускаем
                        if (this.Model.DynamicTable.Meta[colName].DefaultValue != null)
                            continue;

                        // Если обязательное значение не задано - возврат
                        object value = arg.Row.ItemArray[i];
                        Type t = value.GetType();
                        if (value.ToString().Trim() == string.Empty) return;

                        columns.Add(colName);
                        values.Add(value);
                    }

                    bool success = this.Model.TryInsert(columns.ToArray(), values.ToArray());
                    MessageBox.Show(success ? "Успех" : "Ошибка");
                }
                else
                {
                    string[] columns = this.Table.Columns.Cast<DataColumn>().Select(c => c.ColumnName).ToArray();
                    object[] values = arg.Row.ItemArray.ToArray();

                    bool success = this.Model.TryUpdate(columns, values);
                    MessageBox.Show(success ? "Успех" : "Ошибка");
                }
            };

            this.Table.TableNewRow += (_, arg) =>
            {
                if (this.updateFlag == true) return;

                this.newRow = arg.Row;
            };

            this.UpdateCashierViewModels();
            this.UpdateProductViewModels();
            this.UpdateSaleViewModels();
            this.SelectedTableIndex = 0;
            this.SelectedSaleIndex = 0;
        }
     
        void FillTable(DataTable sourceTable)
        {
            this.updateFlag = true;

            this.Table.Columns.Clear();
            this.Table.Clear();

            // Снова заполним колонки
            foreach (DataColumn sourceColumn in sourceTable.Columns)
            {
                DataColumn column = this.Table.Columns.Add(sourceColumn.ColumnName, sourceColumn.DataType);
            }

            DataColumn pkeyColumn = this.Table.Columns[sourceTable.PrimaryKey.First().ColumnName];
            pkeyColumn.ReadOnly = true;

            // Заполним таблицу данными
            foreach (DataRow row in sourceTable.Rows)
            {
                DataRow newRow = this.Table.NewRow();
                for (int i = 0; i < row.ItemArray.Length; i++)
                    newRow[i] = row.ItemArray[i];
                this.Table.Rows.Add(newRow);
            }
            
            this.newRow = null;
            this.updateFlag = false;
        }

        void UpdateCashierViewModels()
        {
            List<ElementViewModel> elements = new List<ElementViewModel>();

            foreach (Cashier cashier in this.Model.Cashiers)
            {
                int pkeyValue = cashier.Id;
                string name = cashier.Name;

                elements.Add(new ElementViewModel() { Name = name, PrimaryKey = pkeyValue });
            }

            int previousSelection = this.SelectedCashierIndex;
            this.CashierViewModels = elements;
            this.SelectedCashierIndex = previousSelection;
        }

        void UpdateProductViewModels()
        {
            List<ProductSelectionViewModel> productSelectionItems = new List<ProductSelectionViewModel>();
            List<ProductInSaleViewModel> productsInSale = new List<ProductInSaleViewModel>();

            foreach (Product product in this.Model.Products)
            {
                int pkeyValue = product.Id;
                string name = $"{product.Name} ({product.Price}р.)";

                ProductSelectionViewModel productSelectionVM = new ProductSelectionViewModel()
                {
                    Name = name,
                    PrimaryKey = pkeyValue,
                    Price = product.Price
                };

                productSelectionVM.PropertyChanged += (obj, arg) =>
                {
                    PreviewNewSaleCost();
                };

                productSelectionItems.Add(productSelectionVM);
            }

            this.ProductSelectionViewModels = productSelectionItems;
            this.PreviewNewSaleCost();
        }

        void PreviewNewSaleCost()
        {
            Dictionary<int, int> productMap = new Dictionary<int, int>();

            foreach (ProductSelectionViewModel productVM in this.ProductSelectionViewModels)
            {
                if (productVM.IsSelected)
                    productMap.Add(productVM.PrimaryKey, productVM.Count);
            }

            this.SaleCost = this.Model.CalculateSaleCost(productMap);
        }

        void UpdateSaleViewModels()
        {
            List<SaleViewModel> elements = new List<SaleViewModel>();

            foreach (Sale sale in this.Model.Sales)
            {
                int pkeyValue = sale.Id;
                string name = sale.Date.ToShortDateString();

                int cost = this.Model.GetSaleCost(pkeyValue);

                SaleViewModel saleVM = new SaleViewModel()
                {
                    Name = name,
                    PrimaryKey = pkeyValue,
                    Cost = cost
                };

                elements.Add(saleVM);
            }

            int previousSelection = this.SelectedSaleIndex;
            this.SaleViewModels = elements;
            this.SelectedSaleIndex = previousSelection;
        }

        void UpdateProductInSaleViewModels()
        {
            if (this.SelectedSaleIndex == -1) return;

            int saleId = this.SaleViewModels[this.SelectedSaleIndex].PrimaryKey;
            
            var products = this.Model.GetProductsInSale(saleId);
            List<ProductInSaleViewModel> productsVM = new List<ProductInSaleViewModel>();

            foreach (var pair in products)
            {
                ProductInSaleViewModel productVM = new ProductInSaleViewModel()
                {
                    Name = pair.Key.Name,
                    Count = pair.Value
                };
                productsVM.Add(productVM);
            }

            this.ProductInSaleViewModels = productsVM;
        }
    }
}
