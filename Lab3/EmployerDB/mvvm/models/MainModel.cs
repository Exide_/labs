﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.Data;
using EmployerDB.pgsql;
using EmployerDB.misc;
using EmployerDB.services;
using EmployerDB.dao;
using Ninject;

namespace EmployerDB.mvvm.models
{
    public class MainModel: BindableBase
    {
        ISaleService saleService;
        IRawDbManagerService rawService;

        public ExtendedTableInfo DynamicTable => this.rawService.DynamicTable;

        public IEnumerable<Cashier> Cashiers => this.saleService.Cashiers;
        public IEnumerable<Product> Products => this.saleService.Products;
        public IEnumerable<Sale> Sales => this.saleService.Sales;
        public IEnumerable<ProductInSale> ProductsInSale => this.saleService.ProductsInSale;

        public string[] TableNames => this.rawService.TableNames;

        public MainModel(ISaleService saleService, IRawDbManagerService rawService)
        {
            this.saleService = saleService;
            this.rawService = rawService;

            this.UpdateAllTables();
        }

        public void SelectTable(int index)
        {
            this.rawService.SelectTable(index);
            this.RaisePropertyChanged(nameof(DynamicTable));
        }

        public void ShowId(int id)
        {
            this.rawService.SelectTable(targetId: id);
            this.RaisePropertyChanged(nameof(DynamicTable));
        }

        public void DeleteId(int id)
        {
            this.rawService.Delete(id);

            this.UpdateAllTables();
        }

        public bool TryInsert(string[] columns, object[] values)
        {
            Dictionary<string, object> valueMap = new Dictionary<string, object>();

            for (int i = 0; i < columns.Length; i++)
                valueMap.Add(columns[i], values[i]);

            bool success = this.rawService.TryInsert(valueMap);

            if (success)
                this.UpdateAllTables();

            return success;
        }

        public bool TryUpdate(string[] columns, object[] values)
        {
            Dictionary<string, object> valueMap = new Dictionary<string, object>();

            for (int i = 0; i < columns.Length; i++)
                valueMap.Add(columns[i], values[i]);

            bool success = this.rawService.TryUpdate(valueMap);

            if (success)
                this.UpdateAllTables();

            return success;
        }

        #region Create sale framework (DONE)
        void UpdateAllTables()
        {
            this.RaisePropertyChanged(nameof(Cashiers));
            this.RaisePropertyChanged(nameof(Products));
            this.RaisePropertyChanged(nameof(Sales));
            this.RaisePropertyChanged(nameof(ProductsInSale));
            this.RaisePropertyChanged(nameof(DynamicTable));
        }

        public bool TryAddSale(int cashierPKey, Dictionary<int, int> productKeyToCountMap)
        {
            bool success = this.saleService.TryAddSale(cashierPKey, productKeyToCountMap);

            if (success)
                this.UpdateAllTables();

            return success;
        }

        public Dictionary<Product, int> GetProductsInSale(int saleId)
        {
            return this.saleService.GetProductsInSale(saleId);
        }

        public int CalculateSaleCost(Dictionary<int, int> productToCountMap)
        {
            return this.saleService.CalculateSaleSum(productToCountMap);
        }

        public int GetSaleCost(int saleId)
        {
            return this.saleService.GetSaleCost(saleId);
        }
        #endregion
    }
}
