﻿using EmployerDB.dao;
using EmployerDB.misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.mvvm.models
{
    public class Cashier: AbstractDbEntity
    {
        public int Id { get; private set; } = -1;
        public string Name { get; private set; } = null;
        public string Passport { get; private set; } = null;
        public int Salary { get; private set; } = -1;

        public Cashier(string name, string passport, int salary)
        {
            this.Name = name;
            this.Passport = passport;
            this.Salary = salary;
        }

        public Cashier()
        {

        }

        public Cashier(Dictionary<string, object> valueMap): base(valueMap)
        {

        }

        public override Dictionary<string, object> GetValues()
        {
            Dictionary<string, object> valueMap = new Dictionary<string, object>();

            valueMap.Add(Schema.Cashier.ID, this.Id);
            valueMap.Add(Schema.Cashier.NAME, this.Name);
            valueMap.Add(Schema.Cashier.PASSPORT, this.Passport);
            valueMap.Add(Schema.Cashier.SALARY, this.Salary);

            return valueMap;
        }

        public override void FromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey(Schema.Cashier.ID))
                this.Id =(int)values[Schema.Cashier.ID];
            
            this.Name = (string)values[Schema.Cashier.NAME];
            this.Passport = (string)values[Schema.Cashier.PASSPORT];
            this.Salary = (int)values[Schema.Cashier.SALARY];
        }

        public override bool IsValid()
        {
            return this.Id > 0;
        }
    }
}
