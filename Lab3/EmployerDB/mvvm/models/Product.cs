﻿using EmployerDB.dao;
using EmployerDB.misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.mvvm.models
{
    public class Product: AbstractDbEntity
    {
        public int Id { get; private set; } = -1;
        public string Name { get; private set; } = null;
        public int Price { get; private set; } = -1;

        public Product(string name, int price)
        {
            this.Name = name;
            this.Price = price;
        }

        public Product()
        {

        }

        public Product(Dictionary<string, object> valueMap): base(valueMap)
        {

        }

        public override Dictionary<string, object> GetValues()
        {
            Dictionary<string, object> valueMap = new Dictionary<string, object>();

            valueMap.Add(Schema.Product.ID, this.Id);
            valueMap.Add(Schema.Product.NAME, this.Name);
            valueMap.Add(Schema.Product.PRICE, this.Price);

            return valueMap;
        }

        public override void FromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey(Schema.Product.ID))
                this.Id = (int)values[Schema.Product.ID];

            this.Name = (string)values[Schema.Product.NAME];
            this.Price = (int)values[Schema.Product.PRICE];
        }

        public override bool IsValid()
        {
            return this.Id > 0;
        }
    }
}
