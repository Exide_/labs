﻿using EmployerDB.dao;
using EmployerDB.misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.mvvm.models
{
    public class ProductInSale: AbstractDbEntity
    {
        public int Id { get; private set; } = -1;
        public int SaleId { get; private set; } = -1;
        public int ProductId { get; private set; } = -1;
        public int Count { get; private set; } = -1;

        public ProductInSale(int saleKey, int productKey, int count)
        {
            this.SaleId = saleKey;
            this.ProductId = productKey;
            this.Count = count;
        }

        public ProductInSale()
        {

        }

        public ProductInSale(Dictionary<string, object> valueMap): base(valueMap)
        {

        }

        public override Dictionary<string, object> GetValues()
        {
            Dictionary<string, object> valueMap = new Dictionary<string, object>();

            valueMap.Add(Schema.ProductInSale.ID, this.Id);
            valueMap.Add(Schema.ProductInSale.SALE_ID, this.SaleId);
            valueMap.Add(Schema.ProductInSale.PRODUCT_ID, this.ProductId);
            valueMap.Add(Schema.ProductInSale.COUNT, this.Count);

            return valueMap;
        }

        public override void FromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey(Schema.ProductInSale.ID))
                this.Id = (int)values[Schema.ProductInSale.ID];

            this.SaleId = (int)values[Schema.ProductInSale.SALE_ID];
            this.ProductId = (int)values[Schema.ProductInSale.PRODUCT_ID];
            this.Count = (int)values[Schema.ProductInSale.COUNT];
        }

        public override bool IsValid()
        {
            return this.Id > 0;
        }
    }
}
