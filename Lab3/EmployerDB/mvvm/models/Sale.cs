﻿using EmployerDB.dao;
using EmployerDB.misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.mvvm.models
{
    public class Sale: AbstractDbEntity
    {
        public int Id { get; private set; } = -1;
        public DateTime Date { get; private set; }
        public int CashierId { get; private set; } = -1;

        public Sale(int cashierId)
        {
            this.CashierId = cashierId;
        }

        public Sale()
        {

        }

        public Sale(Dictionary<string, object> valueMap): base(valueMap)
        {

        }

        public override Dictionary<string, object> GetValues()
        {
            Dictionary<string, object> valueMap = new Dictionary<string, object>();

            valueMap.Add(Schema.Sale.ID, this.Id);
            valueMap.Add(Schema.Sale.SALE_DATE, this.Date);
            valueMap.Add(Schema.Sale.CASHIER_ID, this.CashierId);

            return valueMap;
        }

        public override void FromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey(Schema.Sale.ID))
                this.Id = (int)values[Schema.Sale.ID];

            if (values.ContainsKey(Schema.Sale.SALE_DATE))
                this.Date = (DateTime)values[Schema.Sale.SALE_DATE];
            
            this.CashierId = (int)values[Schema.Sale.CASHIER_ID];
        }

        public override bool IsValid()
        {
            return this.Id > 0;
        }
    }
}
