﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.dao
{
    public abstract class AbstractDbEntity
    {
        public AbstractDbEntity()
        {

        }

        public AbstractDbEntity(Dictionary<string, object> valueMap)
        {
            this.FromValues(valueMap);
        }

        public abstract Dictionary<string, object> GetValues();
        public abstract void FromValues(Dictionary<string, object> values);
        public abstract bool IsValid();

    }
}
