﻿using EmployerDB.misc;
using EmployerDB.mvvm.models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.dao
{
    public class ProductInSaleDAO : Repository<ProductInSale>
    {
        public ProductInSaleDAO(NpgsqlConnection conn) : base(Schema.ProductInSale.TABLE_NAME, conn)
        {

        }

        public int AddProductInSale(ProductInSale productInSale)
        {
            return this.Insert(productInSale);
        }

        public void DeleteProductInSale(ProductInSale productInSale)
        {
            this.Delete(productInSale.Id);
        }
    }
}
