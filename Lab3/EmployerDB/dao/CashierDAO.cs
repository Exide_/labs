﻿using EmployerDB.misc;
using EmployerDB.mvvm.models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.dao
{
    public class CashierDAO: Repository<Cashier>
    {
        public CashierDAO(NpgsqlConnection conn): base(Schema.Cashier.TABLE_NAME, conn)
        {

        }
    }
}
