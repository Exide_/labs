﻿using EmployerDB.misc;
using EmployerDB.mvvm.models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.dao
{
    public class ProductDAO : Repository<Product>
    {
        public ProductDAO(NpgsqlConnection conn): base(Schema.Product.TABLE_NAME, conn)
        {

        }
    }
}
