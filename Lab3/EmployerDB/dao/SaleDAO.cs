﻿using EmployerDB.misc;
using EmployerDB.mvvm.models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.dao
{
    public class SaleDAO : Repository<Sale>
    {
        public SaleDAO(NpgsqlConnection conn): base(Schema.Sale.TABLE_NAME, conn)
        {

        }

        public int AddSale(Sale sale)
        {
            return this.Insert(sale);
        }

        public void DeleteSale(int saleId)
        {
            this.Delete(saleId);
        }
    }
}
