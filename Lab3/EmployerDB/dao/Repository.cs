﻿using EmployerDB.pgsql;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.dao
{
    public interface IRepository
    {
        bool Update(object entity);
        int Insert(object entity);
        bool Delete(object entity);
        bool Delete(int id);
        object GetById(int id);
        List<object> GetAll();
        Type GenericType { get; }
        DataTable GetRawData(int id = -1);
    }

    public class Repository<T>: IRepository where T: AbstractDbEntity
    {
        public NpgsqlConnection Connection { get; }
        public PgsqlTableInfo TableMeta { get; }

        public string TableName => this.TableMeta.Name;

        // Правильнее получать там, где надо, с помощью рефлексии,
        // т.к. нигде в классе не используется
        public Type GenericType => typeof(T); 

        public Repository(string tableName, NpgsqlConnection conn)
        {
            this.Connection = conn;
            this.TableMeta = new PgsqlTableInfo(tableName, conn);
        }

        #region Generic methods
        public List<T> GetAll()
        {
            string sql =
                $"SELECT * FROM {this.TableMeta.Name} " +
                $"ORDER BY {this.TableMeta.PrimaryKeyColumn}";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, this.Connection);
            DataTable tableData = null;

            //try
            //{
                tableData = this.ExecuteReader(cmd);
            //}
            //catch
            //{
            //    return null;
            //}

            List<T> elements = new List<T>();

            for (int i = 0; i < tableData.Rows.Count; i++)
            {
                Dictionary<string, object> valueMap = new Dictionary<string, object>();
                DataRow row = tableData.Rows[i];

                T element = CreateInstance(row);
                elements.Add(element);
            }

            return elements;
        }

        protected bool Update(T entity)
        {
            /*Dictionary<string, object> valueMap = entity.GetValues();

            List<string> columns = valueMap.Keys.ToList();
            List<object> values = valueMap.Values.ToList();
            List<string> placeHolders = columns.Select(col => $"@{col}").ToList();*/

            // 
            //GetParts(entity, out var columns, out var values, out var placeHolders, out var pkeyIndex);

            // Узнаем индекс первичного ключа
            //int pkeyIndex = GetIndex(columns, this.TableMeta.PrimaryKeyColumn);

            // И удалим его из всех списков
            //columns.RemoveRange(pkeyIndex, 1);
            //values.RemoveRange(pkeyIndex, 1);
            //placeHolders.RemoveRange(pkeyIndex, 1);

            EntityInfo<T> entityInfo = new EntityInfo<T>(entity, this.TableMeta);

            var columns = entityInfo.Columns;
            var values = entityInfo.Values;
            var placeHolders = entityInfo.PlaceHolders;
            int pkeyIndex = entityInfo.PKeyIndex;


            StringBuilder builder = new StringBuilder($"UPDATE {this.TableMeta.Name} SET ");
            bool isFirst = true;

            for (int i = 0; i < columns.Count; i++)
            {
                if (i == pkeyIndex) continue;
                if (isFirst == false) builder.Append(", ");
                
                builder.Append($"{columns[i]} = {placeHolders[i]} ");
                isFirst = false;
            }

            builder.Append($"WHERE {columns[pkeyIndex]} = {placeHolders[pkeyIndex]}");


            NpgsqlCommand cmd = new NpgsqlCommand(builder.ToString(), this.Connection);

            for (int i = 0; i < columns.Count; i++)
                cmd.Parameters.AddWithValue(placeHolders[i], values[i]);


            //try
            //{
                ExecuteNonQuery(cmd);
                return true;
            //}
            //catch
            //{
            //    return false;
            //}
        }

        protected int Insert(T entity)
        {
            //GetParts(entity, out var columns, out var values, out var placeHolders, out var pkeyIndex);
            EntityInfo<T> entityInfo = new EntityInfo<T>(entity, this.TableMeta);

            if (entity.IsValid())
                throw new Exception($"Нельзя вставить элемент, полученный из БД (ID: {entityInfo.PKeyValue})");

            var columns = entityInfo.Columns;
            var values = entityInfo.Values;
            var placeHolders = entityInfo.PlaceHolders;

            // Удалим элементы, имеющие значения по умолчанию
            for (int i = columns.Count - 1; i >= 0; i--)
            {
                string column = columns[i];
                bool hasDefaultValue = this.TableMeta[column].DefaultValue != null;

                // Если есть значение - удалим элемент по текущему индексу изо всех списков
                if (hasDefaultValue)
                {
                    columns.RemoveAt(i);
                    values.RemoveAt(i);
                    placeHolders.RemoveAt(i);
                }
            }


            string sql = 
                $"INSERT INTO {this.TableMeta.Name} ({string.Join(",", columns)}) " +
                $"VALUES ({string.Join(",", placeHolders)}) RETURNING id";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, this.Connection);

            for (int i = 0; i < columns.Count; i++)
                cmd.Parameters.AddWithValue(placeHolders[i], values[i]);


            //try
            //{
                return (int)this.ExecuteScalar(cmd);
            //}
            //catch (Exception ex)
            //{
            //    return -1;
            //}
        }

        protected bool Delete(int id)
        {
            string pkeyColumn = this.TableMeta.PrimaryKeyColumn;
            string pkeyPlaceHolder = $"@{pkeyColumn}";

            string sql =
                $"DELETE FROM {this.TableMeta.Name} " +
                $"WHERE {pkeyColumn} = {pkeyPlaceHolder}";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, this.Connection);

            cmd.Parameters.AddWithValue(pkeyPlaceHolder, id);

            //try
            //{
            this.ExecuteNonQuery(cmd);
            return true;
            //}
            //catch
            //{
            //    return false;
            //}
        }

        protected bool Delete(T entity)
        {
            EntityInfo<T> entityInfo = new EntityInfo<T>(entity, this.TableMeta);
            return this.Delete((int)entityInfo.PKeyValue);
        }

        protected T GetById(int id)
        {
            string sql = 
                $"SELECT * FROM {this.TableMeta.Name} " +
                $"WHERE {this.TableMeta.PrimaryKeyColumn} = {id}";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, this.Connection);
            DataTable tableData = this.ExecuteReader(cmd);

            T element = CreateInstance(tableData.Rows[0]);
            return element;
        }
        #endregion

        #region IRepository non-generic implementation
        bool IRepository.Update(object entity)
        {
            return this.Update((T)entity);
        }

        int IRepository.Insert(object entity)
        {
            return this.Insert((T)entity);
        }

        bool IRepository.Delete(object entity)
        {
            return this.Delete((T)entity);
        }

        bool IRepository.Delete(int id)
        {
            return this.Delete(id);
        }

        object IRepository.GetById(int id)
        {
            return this.GetById(id);
        }

        List<object> IRepository.GetAll()
        {
            return this.GetAll().Cast<object>().ToList();
        }

        DataTable IRepository.GetRawData(int id = -1)
        {
            string pkeyColumn = this.TableMeta.PrimaryKeyColumn;
            string sql = $"SELECT * FROM {this.TableMeta.Name}";

            if (id > 0)
                sql += $" WHERE {pkeyColumn} = {id}";

            sql += $" ORDER BY {pkeyColumn}";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, this.Connection);
            DataTable tableData = null;

            //try
            //{
            tableData = this.ExecuteReader(cmd);
            //}
            //catch
            //{
            //    return null;
            //}

            return tableData;
        }
        #endregion

        T CreateInstance(DataRow row)
        {
            Dictionary<string, object> valueMap = new Dictionary<string, object>();

            foreach (DataColumn dataColumn in row.Table.Columns)
            {
                string column = dataColumn.ColumnName;
                valueMap.Add(column, row.Field<object>(column));
            }

            T element = (T)Activator.CreateInstance(typeof(T));
            element.FromValues(valueMap);

            return element;
        }

        #region DB interaction framework
        int ExecuteNonQuery(NpgsqlCommand cmd)
        {
            using (cmd)
                return cmd.ExecuteNonQuery();
        }

        object ExecuteScalar(NpgsqlCommand cmd)
        {
            using (cmd)
            {
                return cmd.ExecuteScalar();
            }
                
        }

        DataTable ExecuteReader(NpgsqlCommand cmd)
        {
            DataTable dt = new DataTable();

            using (cmd)
            {
                using (var reader = cmd.ExecuteReader())
                    dt.Load(reader);
            }

            return dt;
        }
        #endregion

        private class EntityInfo<T> where T: AbstractDbEntity
        {
            public T Entity { get; }
            public List<string> Columns { get; }
            public List<object> Values { get; }
            public List<string> PlaceHolders { get; }
            public int PKeyIndex { get; }

            public string PKeyColumn => this.Columns[PKeyIndex];
            public object PKeyValue => this.Values[PKeyIndex];
            public string PKeyPlaceHolder => this.PlaceHolders[PKeyIndex];

            public EntityInfo(T entity, PgsqlTableInfo tableMeta)
            {
                Dictionary<string, object> valueMap = entity.GetValues();

                this.Columns = valueMap.Keys.ToList();
                this.Values = valueMap.Values.ToList();
                this.PlaceHolders = this.Columns.Select(col => $"@{col}").ToList();

                this.PKeyIndex = GetIndex(this.Columns, tableMeta.PrimaryKeyColumn);
            }

            int GetIndex(IEnumerable<string> values, string value)
            {
                for (int i = 0; i < values.Count(); i++)
                    if (values.ElementAt(i) == value) return i;
                return -1;
            }
        }
    }
}
