﻿using EmployerDB.misc;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.services
{
    public  interface IRawDbManagerService
    {
        string[] TableNames { get; }
        ExtendedTableInfo DynamicTable { get; }

        void SelectTable(int tableIndex = -1, int targetId = -1);
        void Delete(int id);

        bool TryInsert(Dictionary<string, object> valueMap);
        bool TryUpdate(Dictionary<string, object> valueMap);
    }
}
