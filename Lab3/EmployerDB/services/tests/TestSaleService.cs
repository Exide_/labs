﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmployerDB.mvvm.models;
using Ninject;

namespace EmployerDB.services.tests
{
    class TestSaleService : ISaleService
    {
        public int CalculateSaleSum(Dictionary<int, int> productToCountMap)
        {
            // Допустим мы тестируем форматирование больших чисел в представлении
            return 1234567;
        }

        public int GetSaleCost(int saleId)
        {
            // Люблю тестировать форматирование больших чисел
            return 12356789;
        }

        public Dictionary<Product, int> GetProductsInSale(int saleId)
        {
            // Большие числа - круто
            var dict = new Dictionary<Product, int>();
            dict.Add(new Product("Кукиш с маслом", 123456789), 1234567890);

            return dict;
        }

        public bool TryAddSale(int cashierPKey, Dictionary<int, int> productKeyToCountMap)
        {
            // Тестируем реакцию представления на ошибку
            return false;
        }

        // А свойства пусть просто пробрасывают данные из оригинального сервиса, почему бы и нет, не их же мы тестируем :D
        [Inject]
        public SaleService service { get; set; }

        public IEnumerable<Cashier> Cashiers => this.service.Cashiers;
        public IEnumerable<Product> Products => this.service.Products;
        public IEnumerable<Sale> Sales => this.service.Sales;
        public IEnumerable<ProductInSale> ProductsInSale => this.service.ProductsInSale;
    }
}
