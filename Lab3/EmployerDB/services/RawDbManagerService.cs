﻿using EmployerDB.dao;
using EmployerDB.misc;
using EmployerDB.mvvm.models;
using EmployerDB.pgsql;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.services
{
    public class RawDbManagerService: IRawDbManagerService
    {
        public string[] TableNames => this.managerMap.Values.Select(m => m.LocalizedName).ToArray();
        public ExtendedTableInfo DynamicTable { get; private set; }

        private NpgsqlConnection conn;
        private Dictionary<string, TableManager> managerMap = new Dictionary<string, TableManager>();
        private TableManager currentManager = null;

        public RawDbManagerService(
            NpgsqlConnection conn,
            CashierDAO cashierDao,
            ProductDAO productDao,
            SaleDAO saleDao,
            ProductInSaleDAO productInSaleDao)
        {
            this.conn = conn;

            this.managerMap.Add(
                Schema.Cashier.TABLE_NAME,
                new TableManager(
                    Schema.Cashier.TABLE_NAME,
                    "Кассиры", 
                    cashierDao));

            this.managerMap.Add(
                Schema.Product.TABLE_NAME,
                new TableManager(
                    Schema.Product.TABLE_NAME,
                    "Продукты",
                    productDao));

            this.managerMap.Add(
                Schema.Sale.TABLE_NAME,
                new TableManager(
                    Schema.Sale.TABLE_NAME,
                    "Продажи",
                    saleDao));

            this.managerMap.Add(
                Schema.ProductInSale.TABLE_NAME,
                new TableManager(
                    Schema.ProductInSale.TABLE_NAME,
                    "Продукты в продаже",
                    productInSaleDao));
        }

        void UpdateExposedTable(int targetId = -1)
        {
            PgsqlTableInfo metaInfo = new PgsqlTableInfo(currentManager.TableName, this.conn);

            DataTable rawDataTable = currentManager.DAO.GetRawData(targetId);
            rawDataTable.TableName = currentManager.TableName;
            rawDataTable.PrimaryKey = new DataColumn[] { rawDataTable.Columns[metaInfo.PrimaryKeyColumn] };

            this.DynamicTable = new ExtendedTableInfo(metaInfo, rawDataTable);
        }

        public void SelectTable(int tableIndex = -1, int targetId = -1)
        {
            string tableName = 
                tableIndex >= 0 ? 
                this.managerMap.Values.Select(m => m.TableName).ToArray()[tableIndex] : 
                this.currentManager.TableName;

            this.currentManager = this.managerMap[tableName];
            this.UpdateExposedTable(targetId);

            this.currentManager = this.managerMap[tableName];
        }

        public void Delete(int id)
        {
            this.currentManager.DAO.Delete(id);
            this.UpdateExposedTable();
        }

        public bool TryInsert(Dictionary<string, object> valueMap)
        {
            object entity = Activator.CreateInstance(
                this.currentManager.DAO.GenericType,
                valueMap);

            int id = this.currentManager.DAO.Insert(entity);
            this.UpdateExposedTable();

            return id > 0;
        }

        public bool TryUpdate(Dictionary<string, object> valueMap)
        {
            object entity = Activator.CreateInstance(
                this.currentManager.DAO.GenericType,
                valueMap);

            bool success = this.currentManager.DAO.Update(entity);
            this.UpdateExposedTable();

            return success;
        }


        DataTable ExecuteReader(string sql)
        {
            DataTable dt = new DataTable();

            try
            {
                using (NpgsqlCommand cmd = new NpgsqlCommand(sql, this.conn))
                {
                    using (var reader = cmd.ExecuteReader())
                        dt.Load(reader);
                }
            }
            catch
            {
                dt = null;
            }

            return dt;
        }

        private class TableManager
        {
            public string TableName { get; }
            public string LocalizedName { get; }
            public IRepository DAO { get; }

            public TableManager(string tableName, string localizedName, IRepository dao)
            {
                this.TableName = tableName;
                this.LocalizedName = localizedName;
                this.DAO = dao;
            }
        }
    }
}
