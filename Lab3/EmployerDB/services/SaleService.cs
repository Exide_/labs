﻿using EmployerDB.dao;
using EmployerDB.misc;
using EmployerDB.mvvm.models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace EmployerDB.services
{
    public class SaleService: ISaleService
    {
        NpgsqlConnection conn;

        CashierDAO cashierDAO = null;
        ProductDAO productDAO = null;
        SaleDAO saleDAO = null;
        ProductInSaleDAO productInSaleDAO = null;

        public SaleService(
            NpgsqlConnection conn, 
            CashierDAO cashierDao, 
            ProductDAO productDao,
            SaleDAO saleDao,
            ProductInSaleDAO productInSaleDao)
        {
            this.conn = conn;

            this.cashierDAO = cashierDao;
            this.productDAO = productDao;
            this.saleDAO = saleDao;
            this.productInSaleDAO = productInSaleDao;
        }

        public IEnumerable<Cashier> Cashiers => this.cashierDAO.GetAll();
        public IEnumerable<Product> Products => this.productDAO.GetAll();
        public IEnumerable<Sale> Sales => this.saleDAO.GetAll();
        public IEnumerable<ProductInSale> ProductsInSale => this.productInSaleDAO.GetAll();

        public int GetSaleCost(int saleId)
        {
            IEnumerable<Product> products = this.Products;

            // Узнаем стоимость с помощью одного огромного нечитаемого запроса на LINQ
            return this.ProductsInSale
                .Where(p => p.SaleId == saleId)
                .Sum(productInSale =>
                {
                    int productId = productInSale.ProductId;
                    Product product = products.First(p => p.Id == productId);
                    return productInSale.Count * product.Price;
                });
        }

        public Dictionary<Product, int> GetProductsInSale(int saleId)
        {
            Dictionary<Product, int> products = new Dictionary<Product, int>();

            foreach (ProductInSale productInSale in this.ProductsInSale)
            {
                int nextSaleId = productInSale.SaleId;

                if (nextSaleId == saleId)
                {
                    int productId = productInSale.ProductId;
                    int count = productInSale.Count;

                    Product product = this.Products.First(p => p.Id == productId);
                    products.Add(product, count);
                }
            }

            return products;
        }

        public bool TryAddSale(int cashierPKey, Dictionary<int, int> productKeyToCountMap)
        {
            try
            {
                using (TransactionScope trans = new TransactionScope())
                {
                    // Вручную добавим транзакцию, т.к. соединение было создано все этого скоупа
                    this.conn.EnlistTransaction(Transaction.Current);

                    Sale newSale = new Sale(cashierPKey);
                    int id = saleDAO.AddSale(newSale);

                    foreach (var pair in productKeyToCountMap)
                    {
                        int productKey = pair.Key;
                        int count = pair.Value;

                        ProductInSale productInSale = new ProductInSale(id, productKey, count);
                        productInSaleDAO.AddProductInSale(productInSale);
                    }

                    trans.Complete();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public int CalculateSaleSum(Dictionary<int, int> productToCountMap)
        {
            Product[] products = this.Products.ToArray();

            int cost = 0;

            foreach (var pair in productToCountMap)
            {
                int productId = pair.Key;
                int productCount = pair.Value;

                Product product = products.First(p => p.Id == productId);
                cost += productCount * product.Price;
            }

            return cost;
        }

        /// <summary>
        /// Вручную удаляем все продукты из продажи, т.к. я говномес и забыл добавить при проектировании бд ON DELETE CASCADE
        /// </summary>
        /// <param name="saleId"></param>
        public void DeleteSale(int saleId)
        {
            // Получим все продукты в продаже
            var products = this.ProductsInSale.Where(p => p.SaleId == saleId);

            // И каждый из них удалим
            foreach (ProductInSale product in products)
                this.productInSaleDAO.DeleteProductInSale(product);

            // Удалим саму продажу
            this.saleDAO.DeleteSale(saleId);
        }
    }
}
