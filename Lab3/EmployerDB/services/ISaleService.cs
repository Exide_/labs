﻿using EmployerDB.mvvm.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.services
{
    public interface ISaleService
    {
        IEnumerable<Cashier> Cashiers { get; }
        IEnumerable<Product> Products { get; }
        IEnumerable<Sale> Sales { get; }
        IEnumerable<ProductInSale> ProductsInSale { get; }

        int GetSaleCost(int saleId);
        Dictionary<Product, int> GetProductsInSale(int saleId);
        bool TryAddSale(int cashierPKey, Dictionary<int, int> productKeyToCountMap);
        int CalculateSaleSum(Dictionary<int, int> productToCountMap);
    }
}
