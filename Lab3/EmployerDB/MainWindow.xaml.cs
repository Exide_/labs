﻿using EmployerDB.mvvm.viewmodels;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EmployerDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (table == null) return;

            var itemSource = table.ItemsSource;
            table.ItemsSource = null;
            table.ItemsSource = itemSource;
        }

        private void table_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            // Зададим вручную привязку таблицы, т.к. viewmodel задаётся извне после инициализации окна
            Binding binding = new Binding("Table");
            binding.Source = (MainViewModel)e.NewValue;
            table.SetBinding(DataGrid.ItemsSourceProperty, binding);
        }
    }
}
