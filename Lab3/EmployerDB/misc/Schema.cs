﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.misc
{
    static class Schema
    {
        public static class Cashier
        {
            public const string TABLE_NAME = "cashier";
            public const string ID = "id";
            public const string NAME = "name";
            public const string PASSPORT = "passport";
            public const string SALARY = "salary";
        }

        public static class Product
        {
            public const string TABLE_NAME = "product";
            public const string ID = "id";
            public const string NAME = "name";
            public const string PRICE = "price";
        }

        public static class Sale
        {
            public const string TABLE_NAME = "sale";
            public const string ID = "id";
            public const string SALE_DATE = "sale_date";
            public const string CASHIER_ID = "cashier_id";
        }

        public static class ProductInSale
        {
            public const string TABLE_NAME = "product_in_sale";
            public const string ID = "id";
            public const string SALE_ID = "sale_id";
            public const string PRODUCT_ID = "product_id";
            public const string COUNT = "count";
        }
    }
}
