﻿using EmployerDB.pgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.misc
{
    public class ExtendedTableInfo
    {
        public PgsqlTableInfo Meta { get; }
        public DataTable Table { get; }
        public string Name => this.Meta.Name;

        public ExtendedTableInfo(PgsqlTableInfo metaInfo, DataTable table)
        {
            this.Meta = metaInfo;
            this.Table = table;
        }
    }
}
