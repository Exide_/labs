﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.pgsql
{
    public class PgsqlColumnInfo
    {
        public string Name { get; }
        public string Type { get; }
        public string DefaultValue { get; }
        public int Character_Maximum_Length { get; }

        public PgsqlColumnInfo(string name, string type, string defaultValue, int char_max_length)
        {
            this.Name = name;
            this.Type = type;
            this.DefaultValue = defaultValue;
            this.Character_Maximum_Length = char_max_length;
        }
    }
}
