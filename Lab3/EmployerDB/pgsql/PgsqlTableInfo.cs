﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.pgsql
{
    public class PgsqlTableInfo
    {
        public string Name {get;}
        public string PrimaryKeyColumn { get; }
        public PgsqlColumnInfo[] Columns { get; }

        public static string GetFixedValue(string value, string column, PgsqlTableInfo table)
            => table[column].Type.Contains("char") ? $"'{value}'" : value;

        public PgsqlColumnInfo this[string colName] => this.Columns.First(c => c.Name == colName);

        public PgsqlTableInfo(string tableName, NpgsqlConnection conn)
        {
            this.Name = tableName;

            // Для получения общей информации используем первый запрос к базе
            string sql = "select column_name, data_type, column_default, character_maximum_length "
                       + $"from INFORMATION_SCHEMA.COLUMNS where table_name = '{tableName}'";
            NpgsqlCommand command = new NpgsqlCommand(sql, conn);

            // Execute the query and obtain a result set
            using (NpgsqlDataReader dr = command.ExecuteReader())
            {
                List<PgsqlColumnInfo> columns = new List<PgsqlColumnInfo>();

                // Output rows
                while (dr.Read())
                {
                    string colName = dr[0] as string;
                    string colType = dr[1] as string;
                    string defaultValue = dr[2] as string;
                    string max_length_as_str = dr[3] as string;
                    int max_length = string.IsNullOrEmpty(max_length_as_str) ? -1 : int.Parse(max_length_as_str);

                    PgsqlColumnInfo column = new PgsqlColumnInfo(colName, colType, defaultValue, max_length);
                    columns.Add(column);
                }

                this.Columns = columns.ToArray();
            }

            // Для получения информации о первичном ключе используем второй запрос к базе
            string pkeySql = "select column_name from INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE "
                           + $"where (table_name || '_pkey') = constraint_name AND table_name = {tableName};";

            NpgsqlCommand pKeyCommand = new NpgsqlCommand(sql, conn);
            this.PrimaryKeyColumn = (string)pKeyCommand.ExecuteScalar();
        }        
    }
}
