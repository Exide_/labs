﻿using EmployerDB.dao;
using EmployerDB.mvvm.models;
using EmployerDB.mvvm.viewmodels;
using EmployerDB.services;
using EmployerDB.services.tests;
using Ninject.Modules;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.ninject
{
    class NinjectMainConfig : NinjectModule
    {
        public override void Load()
        {
            Bind<MainViewModel>().ToSelf();
            Bind<MainModel>().ToSelf();

            Bind<ISaleService>().To<TestSaleService>(); // TODO: Не забыть вернуть SaleService при деплое (ха)
            Bind<IRawDbManagerService>().To<RawDbManagerService>();

            Bind<NpgsqlConnection>().To<NpgsqlConnection>()
                .InSingletonScope()
                .WithConstructorArgument("connectionString", "Server=localhost;User Id=postgres;Password=postgres;Database=shop_db")
                .OnActivation(conn => conn.Open());

            // Используем единственный экземпляр каждого DAO для двух сервисов
            Bind<CashierDAO>().To<CashierDAO>().InSingletonScope();
            Bind<SaleDAO>().To<SaleDAO>().InSingletonScope();
            Bind<ProductDAO>().To<ProductDAO>().InSingletonScope();
            Bind<ProductInSaleDAO>().To<ProductInSaleDAO>().InSingletonScope();
        }
    }
}
