﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.pgsql
{
    public class PgsqlUserInfo
    {
        public string Name { get; }
        public bool IsSuper { get; }
        //public string GroupName { get; }
        public Dictionary<string, string[]> TableGrants { get; }

        public PgsqlUserInfo(string userName, NpgsqlConnection conn)
        {
            this.Name = userName;

            string suserQuery = $"select usesuper from pg_user where usename = '{userName}';";
            NpgsqlCommand suserCommand = new NpgsqlCommand(suserQuery, conn);

            this.IsSuper = (bool)suserCommand.ExecuteScalar();
            string grantee;

            if (this.IsSuper)
                grantee = this.Name;
            else
            {
                string groupRoleQuery = $"select role_name from information_schema.applicable_roles where grantee = '{this.Name}'";
                NpgsqlCommand granteeCommand = new NpgsqlCommand(groupRoleQuery, conn);

                grantee = (string)granteeCommand.ExecuteScalar();
            }

            string tableGrantsQuery = "select table_name, privilege_type "
                                    + "from information_schema.role_table_grants "
                                   + $"where grantee = '{grantee}'";

            NpgsqlCommand grantsCommand = new NpgsqlCommand(tableGrantsQuery, conn);

            using (NpgsqlDataReader reader = grantsCommand.ExecuteReader())
            {
                Dictionary<string, List<string>> tmpTableGrants = new Dictionary<string, List<string>>();

                while (reader.Read())
                {
                    string tableName = (string)reader[0];
                    string privilege = (string)reader[1];

                    if (!tmpTableGrants.ContainsKey(tableName))
                        tmpTableGrants.Add(tableName, new List<string>());

                    tmpTableGrants[tableName].Add(privilege);
                }

                this.TableGrants = tmpTableGrants.ToDictionary(p => p.Key, p => p.Value.ToArray());
            }
        }
    }
}
