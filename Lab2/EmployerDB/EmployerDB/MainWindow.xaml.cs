﻿using EmployerDB.mvvm.viewmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EmployerDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /*
         * 1. Кассир
         *  id, Имя, паспорт, зп
         * 2. Продажи
         *  id, дата продажи, кассир
         * 3. Товар в продаже
         *  id, товар
         * 4. Товар
         *  id, название, цена
         *  
         *  TODO:
         *  Для вставки в таблицу достаточно внести обязательные данные
         */

        MainViewModel VM => (MainViewModel) this.DataContext;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (table == null) return;

            var itemSource = table.ItemsSource;
            table.ItemsSource = null;
            table.ItemsSource = itemSource;
        }
    }
}
