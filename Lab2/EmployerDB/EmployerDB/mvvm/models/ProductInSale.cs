﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.mvvm.models
{
    public class ProductInSale
    {
        public int PrimaryKey { get; }
        public int SaleId { get; }
        public int ProductId { get; }
        public int Count { get; }

        public ProductInSale(int id, int saleKey, int productKey, int count)
        {
            this.PrimaryKey = id;
            this.SaleId = saleKey;
            this.ProductId = productKey;
            this.Count = count;
        }
    }
}
