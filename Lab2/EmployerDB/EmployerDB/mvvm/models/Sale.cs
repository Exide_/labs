﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.mvvm.models
{
    public class Sale
    {
        public int PrimaryKey { get; }
        public string Date { get; }
        public int CashierId { get; }

        public Sale(int saleKey, string date, int cashierId)
        {
            this.PrimaryKey = saleKey;
            this.Date = date;
            this.CashierId = cashierId;
        }
    }
}
