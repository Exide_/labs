﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.mvvm.models
{
    public class Cashier
    {
        public int PrimaryKey { get; }
        public string Name { get; }
        public string Passport { get; }
        public int Salary { get; }

        public Cashier(int cashierKey, string name, string passport, int salary)
        {
            this.PrimaryKey = cashierKey;
            this.Name = name;
            this.Passport = passport;
            this.Salary = salary;
        }
    }
}
