﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.mvvm.models
{
    public class Product
    {
        public int PrimaryKey { get; }
        public string Name { get; }
        public int Price { get; }

        public Product(int productKey, string name, int price)
        {
            this.PrimaryKey = productKey;
            this.Name = name;
            this.Price = price;
        }
    }
}
