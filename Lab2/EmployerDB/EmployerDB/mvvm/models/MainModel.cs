﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.Data;
using EmployerDB.pgsql;

namespace EmployerDB.mvvm.models
{
    public class MainModel: BindableBase
    {
        private Dictionary<string, string> tableMap = new Dictionary<string, string>();

        private ExtendedTableInfo _tableInfo = null;
        private NpgsqlConnection conn;

        public ExtendedTableInfo DynamicTable
        {
            get => _tableInfo;
            set => SetProperty(ref _tableInfo, value);
        }

        private IEnumerable<Cashier> _cashiers = null;
        private IEnumerable<Product> _products = null;
        private IEnumerable<Sale> _sales = null;
        private IEnumerable<ProductInSale> _productsInSale = null;

        public IEnumerable<Cashier> Cashiers
        {
            get => _cashiers;
            set => SetProperty(ref _cashiers, value);
        }

        public IEnumerable<Product> Products
        {
            get => _products;
            set => SetProperty(ref _products, value);
        }

        public IEnumerable<Sale> Sales
        {
            get => _sales;
            set => SetProperty(ref _sales, value);
        }

        public IEnumerable<ProductInSale> ProductsInSale
        {
            get => _productsInSale;
            set => SetProperty(ref _productsInSale, value);
        }

        public string[] TableNames
        {
            get => this.tableMap.Keys.ToArray();
        }

        public MainModel()
        {
            string connstring = "Server=localhost;User Id=postgres;Password=postgres;Database=shop_db";
            this.conn = new NpgsqlConnection(connstring);
            this.conn.Open();

            this.tableMap.Add("Кассиры", "cashier");
            this.tableMap.Add("Продукты", "product");
            this.tableMap.Add("Продажи", "sale");
            this.tableMap.Add("Продукт в продаже", "product_in_sale");

            this.UpdateAllTables();
        }

        #region Table manager framework
        public void SelectTable(int index)
        {
            string tableName = this.tableMap.Values.ToList()[index];

            ShowTable(tableName);
        }
        
        public void ShowId(string pkeyValue)
        {
            ShowTable(this.DynamicTable.Name, pkeyValue);
        }

        public void DeleteId(string pkeyValue)
        {
            string pkeyName = this.DynamicTable.Meta.PrimaryKeyColumn;
            pkeyValue = Coerce(pkeyName, pkeyValue);

            string sql = $"DELETE FROM {this.DynamicTable.Name} WHERE {pkeyName} = {pkeyValue}";

            try
            {
                ExecuteNonQuery(sql);
            }
            catch { }

            this.UpdateAllTables();
            this.ShowTable(this.DynamicTable.Name);
        }

        public bool TryInsert(string[] columns, string[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                PgsqlColumnInfo colInfo = this.DynamicTable.Meta[columns[i]];
                values[i] = colInfo.Type.Contains("char") ? $"'{values[i]}'" : values[i];
            }

            string sql = 
                $"INSERT INTO {this.DynamicTable.Name} ({string.Join(",",columns)}) " +
                $"VALUES({string.Join(",", values)})";

            bool success = false;

            try
            {
                ExecuteNonQuery(sql);
                success = true;
            }
            catch { }

            this.UpdateAllTables();
            this.ShowTable(this.DynamicTable.Name);
            return success;
        }

        public bool TryUpdate(string[] columns, string[] values)
        {
            string tableName = this.DynamicTable.Name;
            
            for (int i = 0; i < values.Length; i++)
            {
                PgsqlColumnInfo colInfo = this.DynamicTable.Meta[columns[i]];
                values[i] = colInfo.Type.Contains("char") ? $"'{values[i]}'" : values[i];
            }

            int pkeyIndex = GetIndex(columns, this.DynamicTable.Meta.PrimaryKeyColumn);
            string pkeyName = columns[pkeyIndex];
            string pkeyValue = values[pkeyIndex];

            var columnList = columns.ToList(); columnList.RemoveRange(pkeyIndex, 1);
            var valueList = values.ToList(); valueList.RemoveRange(pkeyIndex, 1);

            
            StringBuilder builder = new StringBuilder($"UPDATE {tableName} SET ");

            for (int i = 0; i < columnList.Count; i++)
            {
                if (i != 0) builder.Append(", ");
                builder.Append($"{columnList[i]} = {valueList[i]} ");
            }

            builder.Append($"WHERE {pkeyName} = {pkeyValue}");

            bool success = false;

            try
            {
                ExecuteNonQuery(builder.ToString());
                success = true;
            }
            catch { }

            this.UpdateAllTables();
            this.ShowTable(this.DynamicTable.Name);
            return success;
        }
        

        void ShowTable(string tableName, string pkeyValue = null)
        {
            string sql = $"SELECT * FROM {tableName}";
            PgsqlTableInfo metaInfo = new PgsqlTableInfo(tableName, this.conn);
            string pkeyName = metaInfo.PrimaryKeyColumn;

            if (pkeyValue != null && pkeyValue != string.Empty)
            {
                pkeyValue = Coerce(pkeyName, pkeyValue);
                sql += $" WHERE {pkeyName} = {pkeyValue}";
            }

            sql += $" ORDER BY {pkeyName}";

            DataTable table = ExecuteReader(sql);
            table.PrimaryKey = new DataColumn[] { table.Columns[pkeyName] };
            table.TableName = tableName;

            this.DynamicTable = new ExtendedTableInfo(metaInfo, table);
        }

        #endregion

        #region Create sale framework

        void UpdateAllTables()
        {
            // Анонимный метод, т.к. нигде больше он не нужен
            DataTable GetTable(string tableName)
            {
                PgsqlTableInfo info = new PgsqlTableInfo(tableName, this.conn);
                DataTable table = ExecuteReader($"SELECT * FROM {tableName} ORDER BY {info.PrimaryKeyColumn}");
                table.PrimaryKey = new DataColumn[] { table.Columns[info.PrimaryKeyColumn] };

                return table;
            }


            DataTable cashierTable = GetTable("cashier");
            List<Cashier> cashierList = new List<Cashier>();

            foreach (DataRow row in cashierTable.Rows)
                cashierList.Add(
                    new Cashier((int)row["id"], (string)row["name"], (string)row["passport"], (int)row["salary"]));


            DataTable productTabe = GetTable("product");
            List<Product> productList = new List<Product>();

            foreach (DataRow row in productTabe.Rows)
                productList.Add(
                    new Product((int)row["id"], (string)row["name"], (int)row["price"]));


            DataTable saleTable = GetTable("sale");
            List<Sale> saleList = new List<Sale>();

            foreach (DataRow row in saleTable.Rows)
                saleList.Add(
                    new Sale((int)row["id"], row["sale_date"].ToString(), (int)row["cashier_id"]));


            DataTable productInSaleTable = GetTable("product_in_sale");
            List<ProductInSale> productInSaleList = new List<ProductInSale>();

            foreach (DataRow row in productInSaleTable.Rows)
                productInSaleList.Add(
                    new ProductInSale((int)row["id"], (int)row["sale_id"], (int)row["product_id"], (int)row["count"]));


            this._cashiers = cashierList;
            this._products = productList;
            this._sales = saleList;
            this._productsInSale = productInSaleList;

            this.RaisePropertyChanged(nameof(Cashiers));
            this.RaisePropertyChanged(nameof(Products));
            this.RaisePropertyChanged(nameof(Sales));
            this.RaisePropertyChanged(nameof(ProductsInSale));
        }

        public bool TryAddSale(int cashierPKey, Dictionary<int, int> productKeyToCountMap)
        {
            bool success = false;

            try
            {
                // Создаётся новая закупка, используется ключ кассира
                string createSaleSql = $"INSERT INTO sale (cashier_id) VALUES ({cashierPKey}) RETURNING id";
                int saleId = (int)ExecuteScalar(createSaleSql);

                foreach (var pair in productKeyToCountMap)
                {
                    int pkey = pair.Key;
                    int count = pair.Value;

                    string addProductSql =
                        $"INSERT INTO product_in_sale (sale_id, product_id, count) " +
                        $"VALUES ({saleId}, {pkey}, {count})";

                    ExecuteNonQuery(addProductSql);
                    success = true;
                }
            }
            catch { }

            this.UpdateAllTables();
            return success;
        }

        public Dictionary<string, int> GetProductsInSale(int saleId)
        {
            Dictionary<string, int> products = new Dictionary<string, int>();

            foreach (ProductInSale productInSale in this.ProductsInSale)
            {
                int nextSaleId = productInSale.SaleId;

                if (nextSaleId == saleId)
                {
                    int productId = productInSale.ProductId;
                    int count = productInSale.Count;

                    string productName = this.Products.First(p => p.PrimaryKey == productId).Name;
                    products.Add(productName, count);
                }
            }

            return products;
        }

        public int GetSaleCost(int saleId)
        {
            int cost = 0;

            foreach (ProductInSale productInSale in this.ProductsInSale)
            {
                int nextSaleId = productInSale.SaleId;

                if (nextSaleId == saleId)
                {
                    int productId = productInSale.ProductId;
                    int productPrice = this.Products.First(p => p.PrimaryKey == productId).Price;
                    int count = productInSale.Count;

                    cost += productPrice * count;
                }
            }

            return cost;
        }
        #endregion

        #region DB interaction framework
        int ExecuteNonQuery(string sql)
        {
            using (NpgsqlCommand cmd = new NpgsqlCommand(sql, this.conn))
                return cmd.ExecuteNonQuery();
        }

        object ExecuteScalar(string sql)
        {
            using (NpgsqlCommand cmd = new NpgsqlCommand(sql, this.conn))
                return cmd.ExecuteScalar();
        }

        DataTable ExecuteReader(string sql)
        {
            DataTable dt = new DataTable();

            try
            {
                using (NpgsqlCommand cmd = new NpgsqlCommand(sql, this.conn))
                {
                    using (var reader = cmd.ExecuteReader())
                        dt.Load(reader);
                }
            }
            catch
            {
                dt = null;
            }

            return dt;
        }
        #endregion

        #region Utils
        int GetIndex(string[] values, string value)
        {
            for (int i = 0; i < values.Length; i++)
                if (values[i] == value) return i;
            return -1;
        }

        string Coerce(string column, string value)
        {
            bool needWrap = this.DynamicTable.Meta[column].Type.Contains("char");
            return needWrap ? $"'{value}'" : value;
        }

        public class ExtendedTableInfo
        {
            public PgsqlTableInfo Meta { get; }
            public DataTable Table { get; }
            public string Name => this.Meta.Name;

            public ExtendedTableInfo(PgsqlTableInfo metaInfo, DataTable table)
            {
                this.Meta = metaInfo;
                this.Table = table;
            }
        }
        #endregion
    }
}
