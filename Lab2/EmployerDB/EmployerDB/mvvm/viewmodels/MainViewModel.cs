﻿using EmployerDB.mvvm.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using static EmployerDB.mvvm.models.MainModel;

namespace EmployerDB.mvvm.viewmodels
{    
    public class MainViewModel: ViewModelBase<MainModel>
    {
        private bool updateFlag = false;
        
        #region Add sale 
        private List<ElementViewModel> _cashiers = null;
        private List<ProductSelectionViewModel> _products = null;
        
        private int _saleCost = 0;
        private int _selectedCashier = 0;

        public List<ElementViewModel> CashierViewModels
        {
            get => _cashiers;
            set => SetProperty(ref _cashiers, value);
        }

        public int SaleCost
        {
            get => _saleCost;
            set => SetProperty(ref _saleCost, value);
        }

        public int SelectedCashierIndex
        {
            get => _selectedCashier;
            set => SetProperty(ref _selectedCashier, value);
        }

        public List<ProductSelectionViewModel> ProductSelectionViewModels
        {
            get => _products;
            set => SetProperty(ref _products, value);
        }
        #endregion

        #region Show products in sale
        private List<SaleViewModel> _sales = null;
        private List<ProductInSaleViewModel> _productsInSale = null;
        private int _selectedSale = -1;

        public int SelectedSaleIndex
        {
            get => _selectedSale;
            set => SetProperty(ref _selectedSale, value);
        }

        public List<SaleViewModel> SaleViewModels
        {
            get => _sales;
            set => SetProperty(ref _sales, value);
        }

        public List<ProductInSaleViewModel> ProductInSaleViewModels
        {
            get => _productsInSale;
            set => SetProperty(ref _productsInSale, value);
        }
        #endregion

        #region Table manager UI
        private DataTable _table = new DataTable();
        private int _selectedTable = -1;
        private string _idInput = string.Empty;
        private DataRow newRow = null;

        public string[] TableNames
        {
            get => this.Model.TableNames;
        }

        public DataTable Table
        {
            get => _table;
            set => SetProperty(ref _table, value);
        }

        public int SelectedTableIndex
        {
            get => _selectedTable;
            set => SetProperty(ref _selectedTable, value);
        }

        public string IdInput
        {
            get => _idInput;
            set => SetProperty(ref _idInput, value);
        }
        #endregion

        public ICommand ShowIdCommand { get; }
        public ICommand DeleteIdCommand { get; }
        public ICommand CreateSaleCommand { get; }

        public MainViewModel(): base(new MainModel())
        {
            this.Model.PropertyChanged += (obj, arg) =>
            {
                if (arg.PropertyName == nameof(MainModel.DynamicTable))
                {
                    this.FillTable(this.Model.DynamicTable.Table);
                }
                else if (arg.PropertyName == nameof(MainModel.Cashiers))
                {
                    this.UpdateCashierViewModels();
                }
                else if (arg.PropertyName == nameof(MainModel.Products))
                {
                    this.UpdateProductViewModels();
                }
                else if (arg.PropertyName == nameof(MainModel.Sales))
                {
                    this.UpdateSaleViewModels();
                }
            };

            this.PropertyChanged += (obj, arg) =>
            {
                if (arg.PropertyName == nameof(SelectedTableIndex))
                {
                    this.Model.SelectTable(this.SelectedTableIndex);
                }
                else if (arg.PropertyName == nameof(this.SelectedSaleIndex))
                {
                    this.UpdateProductInSaleViewModels();
                }
            };

            this.ShowIdCommand = new DelegateCommand((obj) =>
            {
                this.Model.ShowId(this.IdInput);
            });

            this.DeleteIdCommand = new DelegateCommand((obj) =>
            {
                this.Model.DeleteId(this.IdInput);
            });

            this.CreateSaleCommand = new DelegateCommand((obj) =>
            {
                int cashierPKey = int.Parse(this.CashierViewModels[this.SelectedCashierIndex].PrimaryKey);

                Dictionary<int, int> productMap = new Dictionary<int, int>();

                foreach (ProductSelectionViewModel productVM in this.ProductSelectionViewModels)
                {
                    if (productVM.IsSelected == false) continue;

                    int count = productVM.Count;
                    int productPK = int.Parse(productVM.PrimaryKey);
                    productVM.IsSelected = false;
                    productVM.Count = 1;

                    productMap.Add(productPK, count);
                }

                bool success = this.Model.TryAddSale(cashierPKey, productMap);
                MessageBox.Show(success ? "Успех" : "Ошибка");
            });


            this.Table.RowChanged += (_, arg) =>
            {
                if (this.updateFlag == true) return;

                bool isNew = arg.Row == this.newRow;

                if (isNew == true)
                {
                    List<string> columns = new List<string>();
                    List<string> values = new List<string>();
                    
                    for (int i = 0; i < arg.Row.ItemArray.Length; i++)
                    {
                        string colName = this.Table.Columns[i].ColumnName;
                        // Если для колонки есть значение по умолчанию - пропускаем
                        if (this.Model.DynamicTable.Meta[colName].DefaultValue != null)
                            continue;

                        // Если обязательное значение не задано - возврат
                        string value = arg.Row.ItemArray[i].ToString();
                        if (value == string.Empty) return;

                        columns.Add(colName);
                        values.Add(value);
                    }

                    bool success = this.Model.TryInsert(columns.ToArray(), values.ToArray());
                    MessageBox.Show(success ? "Успех" : "Ошибка");
                }
                else
                {
                    string[] columns = this.Table.Columns.Cast<DataColumn>().Select(c => c.ColumnName).ToArray();
                    string[] values = arg.Row.ItemArray.Select(v => v.ToString()).ToArray();

                    bool success = this.Model.TryUpdate(columns, values);
                    MessageBox.Show(success ? "Успех" : "Ошибка");
                }
            };

            this.Table.TableNewRow += (_, arg) =>
            {
                if (this.updateFlag == true) return;

                this.newRow = arg.Row;
            };

            this.UpdateCashierViewModels();
            this.UpdateProductViewModels();
            this.UpdateSaleViewModels();
            this.SelectedTableIndex = 0;
            this.SelectedSaleIndex = 0;
        }
     
        void FillTable(DataTable sourceTable)
        {
            this.updateFlag = true;

            this.Table.Columns.Clear();
            this.Table.Clear();

            // Снова заполним колонки
            foreach (var col in sourceTable.Columns)
                this.Table.Columns.Add(col.ToString());

            // Заполним таблицу данными
            foreach (DataRow row in sourceTable.Rows)
            {
                DataRow newRow = this.Table.NewRow();
                for (int i = 0; i < row.ItemArray.Length; i++)
                    newRow[i] = row.ItemArray[i];
                this.Table.Rows.Add(newRow);
            }
            
            this.newRow = null;
            this.updateFlag = false;
        }

        void UpdateCashierViewModels()
        {
            List<ElementViewModel> elements = new List<ElementViewModel>();

            foreach (Cashier cashier in this.Model.Cashiers)
            {
                string pkeyValue = cashier.PrimaryKey.ToString();
                string name = cashier.Name;

                elements.Add(new ElementViewModel() { Name = name, PrimaryKey = pkeyValue });
            }

            int previousSelection = this.SelectedCashierIndex;
            this.CashierViewModels = elements;
            this.SelectedCashierIndex = previousSelection;
        }

        void UpdateProductViewModels()
        {
            List<ProductSelectionViewModel> productSelectionItems = new List<ProductSelectionViewModel>();
            List<ProductInSaleViewModel> productsInSale = new List<ProductInSaleViewModel>();

            foreach (Product product in this.Model.Products)
            {
                string pkeyValue = product.PrimaryKey.ToString();
                string name = $"{product.Name} ({product.Price}р.)";

                ProductSelectionViewModel productSelectionVM = new ProductSelectionViewModel()
                {
                    Name = name,
                    PrimaryKey = pkeyValue,
                    Price = product.Price
                };

                productSelectionVM.PropertyChanged += (obj, arg) =>
                {
                    CalculateNewSaleCost();
                };

                productSelectionItems.Add(productSelectionVM);
            }

            this.ProductSelectionViewModels = productSelectionItems;
            this.CalculateNewSaleCost();
        }

        void CalculateNewSaleCost()
        {
            int cost = 0;

            foreach (ProductSelectionViewModel productVM in this.ProductSelectionViewModels)
                cost += productVM.IsSelected ? productVM.Count * productVM.Price : 0;

            this.SaleCost = cost;
        }

        void UpdateSaleViewModels()
        {
            List<SaleViewModel> elements = new List<SaleViewModel>();

            foreach (Sale sale in this.Model.Sales)
            {
                int pkeyValue = sale.PrimaryKey;
                string name = sale.Date;

                int cost = this.Model.GetSaleCost(pkeyValue);

                SaleViewModel saleVM = new SaleViewModel()
                {
                    Name = name,
                    PrimaryKey = pkeyValue.ToString(),
                    Cost = cost
                };

                elements.Add(saleVM);
            }

            int previousSelection = this.SelectedSaleIndex;
            this.SaleViewModels = elements;
            this.SelectedSaleIndex = previousSelection;
        }

        void UpdateProductInSaleViewModels()
        {
            if (this.SelectedSaleIndex == -1) return;

            int saleId = int.Parse(this.SaleViewModels[this.SelectedSaleIndex].PrimaryKey);
            
            var products = this.Model.GetProductsInSale(saleId);
            List<ProductInSaleViewModel> productsVM = new List<ProductInSaleViewModel>();

            foreach (var pair in products)
            {
                ProductInSaleViewModel productVM = new ProductInSaleViewModel()
                {
                    Name = pair.Key,
                    Count = pair.Value
                };
                productsVM.Add(productVM);
            }

            this.ProductInSaleViewModels = productsVM;
        }
    }
}
