﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.mvvm.viewmodels
{
    public class ProductInSaleViewModel: ElementViewModel
    {
        private int _count = 1;

        public int Count
        {
            get => _count;
            set => SetProperty(ref _count, value);
        }

        public int Price { get; set; }
    }
}
