﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployerDB.mvvm.viewmodels
{
    public class ElementViewModel : BindableBase
    {
        public string Name { get; set; }
        public string PrimaryKey { get; set; }

    }
}
