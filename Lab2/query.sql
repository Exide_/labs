1. Кассир
 *  id, Имя, паспорт, зп
 * 2. Продажи
 *  id, дата продажи, кассир
 * 3. Товар в продаже
 *  id, товар
 * 4. Товар
 *  id, название, цена
 
CREATE TABLE cashier
(
	id SERIAL primary key,
	name varchar(50),
	passport varchar(10),
	salary int
);

CREATE TABLE sale
(
	id SERIAL primary key,
	sale_date date DEFAULT CURRENT_DATE,
	cashier_id int references cashier
);

CREATE TABLE product
(
	id SERIAL primary key,
	name varchar(50),
	price int
);

CREATE TABLE product_in_sale
(
	id SERIAL primary key,
	sale_id int references sale,
	product_id int references product
	count int
);